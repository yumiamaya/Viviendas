import java.util.ArrayList;

public class Main {
    public static ArrayList<Viviendas> viviendas;
    public static ArrayList<Propietarios> propietarios;

    public static void main(String[] args) {
        MenuPrincipal main = new MenuPrincipal();
        main.setVisible(true);
        Viviendas v0 = new Piso("VH5797S", 100, 4, 2, 1, 8, 3, 125);
        Viviendas v1 = new Piso("XL1234K", 125, 5, 2, 1, 6, 4, 85);
        Viviendas v2 = new Piso("KL5924M", 60, 2, 1, 2, 4, 4, 45);
        Viviendas v3 = new Piso("FP2545P", 90, 3, 2, 4, 6, 1, 50);
        Viviendas v4 = new Chalet("HJ9587K", 350, 4, 4, 3, 2, 800, true);
        Viviendas v5 = new Chalet("RJ2463P", 290, 4, 3, 3, 3, 650, false);
        Viviendas v6 = new Chalet("HT5797S", 400, 6, 6, 2, 3, 1254.20f, true);
        Viviendas v7 = new Chalet("PJ5799K", 320, 4, 4, 4, 4, 980, false);
        Viviendas v8 = new Adosado("GY5269T", 230, 4, 3, 3, 2, 12, 125);
        Viviendas v9 = new Adosado("RW2985R", 180, 5, 2, 3, 3, 24, 400);
        Viviendas v10 = new Adosado("NX6536D", 200, 5, 3, 4, 4, 20, 250);

        viviendas = new ArrayList<>();
        viviendas.add(v0);
        viviendas.add(v1);
        viviendas.add(v2);
        viviendas.add(v3);
        viviendas.add(v4);
        viviendas.add(v5);
        viviendas.add(v6);
        viviendas.add(v7);
        viviendas.add(v8);
        viviendas.add(v9);
        viviendas.add(v10);

        propietarios = new ArrayList<>();
        propietarios.add(new Propietarios("Luisa Pérez", "12883415B", 35, "C/ Pez, 34", "623 33 22 33", new Viviendas[]{v4, v6, null}));
        propietarios.add(new Propietarios("Juan Castillo", "44105070V", 42, "C/ Ansó, 40", "999 21 45 22", new Viviendas[]{v2, null, null}));
        propietarios.add(new Propietarios("Ana Ruiz", "11889753H", 61, "Avda Madrid, 3", "772 124 254", new Viviendas[]{v10, null, null}));
        propietarios.add(new Propietarios("Eva Monje", "61778120M", 33, "C/ Palo, 123", "345 1111111", new Viviendas[]{v3, null, null}));
        propietarios.add(new Propietarios("Pepe Luis", "10500232L", 20, "Avda Soria, 21", "123 123 123", new Viviendas[]{v1, v7, v9}));
    }
}
