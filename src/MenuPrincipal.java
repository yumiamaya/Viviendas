import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuPrincipal extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("MENU PRINCIPAL");
		lblNewLabel.setBounds(98, 11, 235, 32);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 26));
		contentPane.add(lblNewLabel);
		
		JButton btnAltaCliente = new JButton("ALTA CLIENTE");
		btnAltaCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaCliente altaCliente = new AltaCliente();
				altaCliente.setVisible(true);
			}
		});
		btnAltaCliente.setBounds(114, 75, 203, 35);
		contentPane.add(btnAltaCliente);
		
		JButton btnNewButton = new JButton("ALTA VIVIENDA");
		btnNewButton.setBounds(114, 121, 203, 35);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			    AltaCasa casa = new AltaCasa();
			    casa.setVisible(true);
			}
		});
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("CALCULAR ALQUILER");
		btnNewButton_1.setBounds(114, 167, 203, 35);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CalculoAlquiler pisos = new CalculoAlquiler();
				pisos.setVisible(true);
			}
		});
	}
}
